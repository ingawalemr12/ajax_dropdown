<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class New_UserModel extends CI_Model {

    public function fetch_Country()
	{
		$countries = $this->db->get('countries')->result_array();
		return $countries;
	}

	public function fetch_states($country_id)
	{
		$this->db->where('country_id', $country_id);
		$query= $this->db->get('states');

		$output = '<option value="">Select State</option>';
		foreach ($query->result() as $row) {
			$output .='<option value="'.$row->id.'">'.$row->name.'</option>';
		}
		return $output;
	}

	public function fetch_cities($state_id)
	{
		$this->db->where('state_id', $state_id);
		$cities = $this->db->get('cities');

		$output = '<option value="">Select City</option>';
		foreach ($cities->result() as $city) {
			$output .='<option value="'.$city->id.'">'.$city->city_name.'</option>';
		}
		return $output;
	}

	public function insert_form_record($formdata)
	{
		return $this->db->insert('usertable', $formdata);
	}
	
	public function fetch_form_record()
	{
		$result = $this->db->order_by('id', 'DESC')->get('usertable')->result_array();
		return $result;
	}
}
