<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_Ajax_Controller extends CI_Controller {

	public function index()
	{
		$this->load->view('crud/list');
	}
}
