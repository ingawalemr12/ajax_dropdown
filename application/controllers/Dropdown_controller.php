<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropdown_controller extends CI_Controller {

	public function __construct(){ 
		parent::__construct();
		$this->load->model('New_UserModel');
	}

    public function index()
	{
		$countries = $this->New_UserModel->fetch_Country();
		//echo "<pre>";		print_r($countries);
		$data=array();
		$data['countries']=$countries;
		$this->load->view('new_dropdown_view', $data);
	}

	public function get_states()
	{
		$country_id = $this->input->post('country_id');
		echo $this->New_UserModel->fetch_states($country_id);
		// $states= $this->New_UserModel->fetch_states($country_id);
		// print_r($states);
	}
	
	public function get_cities()
	{
		$state_id = $this->input->post('state_id');
		echo $this->New_UserModel->fetch_cities($state_id);
		// $cities = $this->New_UserModel->fetch_cities($state_id);
		// print_r($cities);
	}

	public function insert_record()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		if ($this->form_validation->run()==TRUE) 
		{	
			# validation success...... insert into table 
			$formdata=array();
			$formdata['name'] = $this->input->post('name');
			$formdata['email'] = $this->input->post('email');
			$formdata['countries'] = $this->input->post('countries');
			$formdata['states'] = $this->input->post('states');
			$formdata['cities'] = $this->input->post('cities');

			$this->New_UserModel->insert_form_record($formdata);
			
			$this->session->set_flashdata('success', 'record inserted successfully');
			redirect(base_url().'Dropdown_controller/fetch_record');
		}
		else
		{	
			# validation errors 
			$this->load->view('new_dropdown_view');
		}
	}

	public function fetch_record()
	{
		$data=array();
		$usertable = $this->New_UserModel->fetch_form_record();
		$data['usertable'] = $usertable;
		// echo "<pre>";		print_r($usertable); exit();
		$this->load->view('new_dropdown_list', $data);
	}
}
