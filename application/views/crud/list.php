
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
  <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap.min.css">
  <script src="<?php echo base_url() ?>/assets/jquery-3.4.1.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css'); ?>"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <title>Codeigniter 3 Ajax CRUD Application</title>
    
</head>
<body>
<div class="container">
  <h2 class="text-center mt-4 mb-4">Codeigniter 3 Ajax CRUD Application</h2>
  <span id="message"></span>  
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col">Sample Data</div>
          <div class="col text-right">
              <a href="javascript:void(0);" onclick="showModal()" class="btn btn-success btn-sm">Add</a>
          </div>
      </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered" id="sample_table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>

  </div>
</div>

  <!-- Modal to insert data in table -->
  <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">AJAX CRUD Function</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="response">
          
        </div>
      </div>
    </div>
  </div>
</body>

<script type="text/javascript">
  $("document").ready(function(){
    
    function showModal(){
      $("#createModal").modal(show);
    }

  });
</script>

</html>