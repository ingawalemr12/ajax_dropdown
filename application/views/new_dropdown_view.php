<!DOCTYPE html>
<html>
<head>
	<title>AJAX Drop-Down Menu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap.min.css">
</head>
<body>

<div class="container pt-3">
  <div class="row">
    <div class="col-md-12">
      <h2 class="text-center bg-dark text-white pb-2">AJAX Drop-Down Menu </h2>
    </div>
    
    <div class="col-md-12">
      <form method="post" action="<?php echo base_url('Dropdown_controller/insert_record'); ?>">
        
        <div class="form-group row">
            <label class="col-sm-3 text-right control-label col-form-label">Name </label>
            <div class="col-sm-9">
               <input type="text" name="name" id="name" class="form-control">
               <?php echo form_error('name'); ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 text-right control-label col-form-label">Email </label>
            <div class="col-sm-9">
               <input type="email" name="email" id="email" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 text-right control-label col-form-label">Country</label>
            <div class="col-sm-9">
              <select name="countries" id="countries" class="form-control">
              	<option value="">Select Country</option>
              	<?php
              	if (!empty($countries)) {
              		foreach ($countries as $country) { ?>
              		 <option value="<?php echo $country['id'] ?>">
              		 	<?php echo $country['name'] ?>
              		 </option>
              		 <?php
              		}
              	}				
              	 ?>
               
              </select>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-sm-3 text-right control-label col-form-label">State</label>
            <div class="col-sm-9">
                <select name="states" id="states" class="form-control">
                  <option value="">Select a State</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 text-right control-label col-form-label">City</label>
            <div class="col-sm-9">
                <select name="cities" id="cities" class="form-control">
                  <option value="">Select a City</option>
                </select>
            </div>
        </div>

        <div class="form-group">
          	<button type="submit" class="btn btn-primary float-right" name="insert">Insert</button>
          	<button type="reset" class="btn btn-default float-right" >Clear</button>
          	<button type="reset" class="btn btn-default float-right" ><a href="<?php echo base_url().'Dropdown_controller/fetch_record' ?>" class="btn btn-success btn-sm">List View</a></button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>

<script src="<?php echo base_url() ?>/assets/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/bootstrap.min.js"></script>

<script type="text/javascript">
	$("document").ready(function () {

		// on change of countries, it select state 
		$("#countries").change(function(){
			var country_id = $("#countries").val();//$(this).val();
			//alert(country_id);

			if (country_id !="") 
			{
				$.ajax({
					url:'<?php echo base_url().'Dropdown_controller/get_states' ?>',
					method:'POST',
					data: {country_id:country_id},
					success:function (data) {
						$("#states").html(data);
						//$("#cities").html('<option value="">Select City</option>');
					}
				});
			}

		});

		// on change of states, it select city 
		$("#states").change(function(){
			var state_id = $(this).val();
			//alert(state_id);
			
			if (state_id !="") 
			{
				$.ajax({
					url:'<?php echo base_url().'Dropdown_controller/get_cities' ?>',
					method:'POST',
					data:{state_id:state_id},
					success:function(data){
						$("#cities").html(data);
					}
				});
			}

		});

	});
</script>
</html>
