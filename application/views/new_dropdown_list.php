
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
  <link rel="stylesheet" href="<?php echo base_url() ?>/assets/bootstrap.min.css">
  <script src="<?php echo base_url() ?>/assets/jquery-3.4.1.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css'); ?>"/>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <title>Codeigniter 3 Ajax CRUD Application</title>
    
</head>
<body>
<div class="container">
  <h2 class="text-center mt-4 mb-4">Codeigniter 3 Ajax CRUD Application</h2>

    <!-- session -->
    <div class="row">
          <div class="col-md-12">
            <?php if (!empty($this->session->flashdata('success'))) { ?>
            <div class="alert alert-success">
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php } ?>
          </div>
    </div>

  <span id="message"></span>  
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col">Sample Data</div>
          <div class="col text-right">
            <a href="<?php echo base_url().'Dropdown_controller' ?>">
              <button type="button" name="add_record" id="add_record" class="btn btn-success btn-sm">Add</button>
            </a>
          </div>
      </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered" id="sample_table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Country</th>
                <th>State</th>
                <th>City</th>
              </tr>
            </thead>
             <tbody>
              <?php
              if (!empty($usertable)) {
                foreach ($usertable as $row) {
                  ?>
                  <tr>
                    <td><?php echo $row['id']  ?></td>
                    <td><?php echo $row['name']  ?></td>
                    <td><?php echo $row['email']  ?></td>
                    <td><?php echo $row['countries']  ?></td>
                    <td><?php echo $row['states']  ?></td>
                    <td><?php echo $row['cities']  ?></td>
                  </tr>
                  <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>

  </div>
</div>
</body>
</html>